<?php
require_once("../../../vendor/autoload.php");
use App\ProfilePicture\ProfilePicture;


$file = $_FILES['file_upload'];
$_POST['file_upload'] = $file;


$objProfilePicture = new ProfilePicture();

$objProfilePicture->setData($_POST);
$objProfilePicture->update();
$objProfilePicture->moveFile();