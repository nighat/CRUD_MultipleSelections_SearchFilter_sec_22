<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

$msg = Message::message();

echo "<div>  <div id='message'>  $msg </div>   </div>";

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Profile Picture Add Form</title>
</head>
<body>
<h2 align="center">Profile Picture Add Form</h2>
<form action="store.php" method="post" enctype="multipart/form-data">

    <table border="1"; width=550 align=center bgcolor="#a9a9a9" >
        <tr> <td>Please  Name:</td>
            <td><input type="text" class="form-control" name="name" placeholder="Type Your Name Here..."></td>
        </tr>
        <tr><td> Please Enter Profile Picture:</td>
            <td><input type="file" name="file_upload"></td>
        </tr>

        <tr><td colspan="2"> <input type="submit"></td></tr>

    </table >
</form>
<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>
</body>
</html>