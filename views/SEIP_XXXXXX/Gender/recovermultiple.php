<?php
require_once("../../../vendor/autoload.php");
use App\Message\Message;
use App\Utility\Utility;
use \App\Gender\Gender;

if(isset($_POST['mark'])) {

    $objGender= new Gender();
    $objGender->recoverMultiple($_POST['mark']);

    Utility::redirect("index.php?Page=1");
}
else
{
    Message::message("Empty Selection! Please select some records.");
    Utility::redirect("trashed.php");

}