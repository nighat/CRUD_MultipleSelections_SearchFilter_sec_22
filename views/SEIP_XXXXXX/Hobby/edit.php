<?php

require_once("../../../vendor/autoload.php");
if(!isset($_SESSION)) session_start();
use App\Message\Message;

$msg = Message::message();

echo "<div>  <div id='message'>  $msg </div>   </div>";

$objHobby = new \App\Hobby\Hobby();
$objHobby->setData($_GET);
$oneData = $objHobby->view();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hobby Add Form</title>
</head>
<body>
<h2 style="text-align:center ">Hobby Add Form</h2>
<form action="update.php" method="post">
    <table border="1"; width=550 align=center bgcolor="teal" >
        <tr><td>Please Enter  Name:</td>
            <td><input type="text" name="name" value="<?php echo $oneData->name?>"></td></tr>
        <tr>
            <td>Please Enter  Hobbies:</td>
            <td><input type="checkbox" name="hobby[]" value="Playing Guiter" value="<?php echo $oneData->hobby?>"> Playing Guiter
                <input type="checkbox" name="hobby[]" value="Reading"> Reading
                <input type="checkbox" name="hobby[]" value="Painting"> Painting
                <input type="checkbox" name="hobby[]" value="Gardenning"> Gardenning </td></tr>

        <tr><td><input type="hidden" name="id" value="<?php echo $oneData->id?>" ></td></tr>


        <tr><td><input class="btn btn-primary" type="submit" value="Update"></td></tr>

    </table >

</form>

<script src="../../../resource/bootstrap/js/jquery.js"></script>

<script>


    jQuery(

        function($) {
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
            $('#message').fadeIn (550);
            $('#message').fadeOut (550);
        }
    )
</script>


</body>
</html>