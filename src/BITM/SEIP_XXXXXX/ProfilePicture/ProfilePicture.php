<?php


namespace App\ProfilePicture;
use App\Model\Database as DB;
use App\Message\Message;
use App\Utility\Utility;
use PDO;
use PDOException;

class ProfilePicture extends DB
{
    private $id;
    private $name;
    private $fileName;
    private $tmpName;



    public function setData($postData) {
        if(array_key_exists('id',$postData)) {
            $this->id = $postData['id'];
        }


        if(array_key_exists('name',$postData)) {
            $this->name = $postData['name'];

        }

        if(array_key_exists('file_upload',$postData)){
            if(array_key_exists("name",$postData)){
                $this->fileName = $postData['file_upload']['name'];
            }
        }
        if(array_key_exists('file_upload',$postData)){
            if(array_key_exists("name",$postData)){
                $this->tmpName = $postData['file_upload']['tmp_name'];
            }
        }
    }

    public function store() {

        $arrData = array($this->name,$this->fileName);
        $sql = "INSERT INTO profile_picture(name,file_name) VALUES (?,?)";
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);



        if($result)
            Message::message("Success! Data Has Been Inserted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Inserted Successfully :( ");

        Utility::redirect('index.php');

    }

    public function moveFile() {
        move_uploaded_file($this->tmpName, "uploads/".$this->fileName);
    }

    public function index(){

        $sql = "select * from profile_picture where soft_deleted='No'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }


    public function view(){

        $sql = "select * from profile_picture where id=".$this->id;

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetch();

    }


    public function trashed(){

        $sql = "select * from profile_picture where soft_deleted='Yes'";

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        return $STH->fetchAll();

    }
    public function update() {

        $arrData = array($this->name,$this->fileName);
        $sql = "UPDATE profile_picture SET name=?,file_name=? WHERE id=".$this->id;
        $statement = $this->DBH->prepare($sql);
        $result = $statement->execute($arrData);



        if($result)
            Message::message("Success! Data Has Been updated Successfully :)");
        else
            Message::message("Failed! Data Has Not Been updated Successfully :( ");

        Utility::redirect('index.php');

    }
    public function trash() {


        $sql = "UPDATE profile_picture SET soft_deleted='Yes' WHERE id=".$this->id;
        $result  = $this->DBH->exec($sql);

        if($result)
            Message::message("Success! Data Has Been Soft_Deleted Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Soft_Deleted Successfully :( ");

        Utility::redirect('index.php');

    }
    public function recover() {


        $sql = "UPDATE profile_picture SET soft_deleted='Yes' WHERE id=".$this->id;
        $result  = $this->DBH->exec($sql);

        if($result)
            Message::message("Success! Data Has Been Recovered  Successfully :)");
        else
            Message::message("Failed! Data Has Not Been Recovered Successfully :( ");

        Utility::redirect('index.php');

    }

    public function delete(){

        $sql = "Delete from profile_picture WHERE id=".$this->id;

        $result = $this->DBH->exec($sql);



        if($result)
            Message::message("Success! Data Has Been Permanently Deleted :)");
        else
            Message::message("Failed! Data Has Not Been Permanently Deleted  :( ");


        Utility::redirect('index.php');


    }

    public function indexPaginator($page=1,$itemsPerPage=3){
        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from profile_picture WHERE soft_deleted = 'No' LIMIT $start,$itemsPerPage";



        }catch (PDOException $error){

            $sql = "SELECT * from profile_picture WHERE soft_deleted = 'No'";

        }

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;


    }



    public function trashedPaginator($page=1,$itemsPerPage=3){

        try{

            $start = (($page-1) * $itemsPerPage);
            if($start<0) $start = 0;
            $sql = "SELECT * from profile_picture WHERE soft_deleted = 'Yes' LIMIT $start,$itemsPerPage";



        }catch (PDOException $error){

            $sql = "SELECT * from profile_picture  WHERE soft_deleted = 'Yes'";

        }

        $STH = $this->DBH->query($sql);

        $STH->setFetchMode(PDO::FETCH_OBJ);

        $arrSomeData  = $STH->fetchAll();
        return $arrSomeData;




    }






    public function trashMultiple($selectedIDsArray){


        foreach($selectedIDsArray as $id){

            $sql = "UPDATE  profile_picture SET soft_deleted='Yes' WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Soft Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Soft Deleted  :( ");


        Utility::redirect('trashed.php?Page=1');


    }


    public function recoverMultiple($markArray){


        foreach($markArray as $id){

            $sql = "UPDATE profile_picture SET soft_deleted='No' WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been Recovered Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Recovered  :( ");


        Utility::redirect('index.php?Page=1');


    }



    public function deleteMultiple($selectedIDsArray){


        foreach($selectedIDsArray as $id){

            $sql = "Delete from profile_picture  WHERE id=".$id;

            $result = $this->DBH->exec($sql);

            if(!$result) break;

        }



        if($result)
            Message::message("Success! All Seleted Data Has Been  Deleted Successfully :)");
        else
            Message::message("Failed! All Selected Data Has Not Been Deleted  :( ");


        Utility::redirect('index.php?Page=1');


    }



    public function listSelectedData($selectedIDs){



        foreach($selectedIDs as $id){

            $sql = "Select * from profile_picture WHERE id=".$id;


            $STH = $this->DBH->query($sql);

            $STH->setFetchMode(PDO::FETCH_OBJ);

            $someData[]  = $STH->fetch();


        }


        return $someData;


    }




    public function search($requestArray){
        $sql = "";
        if( isset($requestArray['byName']) && isset($requestArray['byFile_name']) )  $sql = "SELECT * FROM `profile_picture` WHERE `soft_deleted` ='No' AND (`profile_picture` LIKE '%".$requestArray['search']."%' OR `file_name` LIKE '%".$requestArray['search']."%')";
        if(isset($requestArray['byName']) && !isset($requestArray['byFile_name']) ) $sql = "SELECT * FROM `profile_picture` WHERE `soft_deleted` ='No' AND `profile_picture` LIKE '%".$requestArray['search']."%'";
        if(!isset($requestArray['byName']) && isset($requestArray['byFile_name']) )  $sql = "SELECT * FROM `profile_picture` WHERE `soft_deleted` ='No' AND `profile_picture` LIKE '%".$requestArray['search']."%'";

        $STH  = $this->DBH->query($sql);
        $STH->setFetchMode(PDO::FETCH_OBJ);
        $someData = $STH->fetchAll();

        return $someData;

    }// end of search()




    public function getAllKeywords()
    {
        $_allKeywords = array();
        $WordsArr = array();

        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->name);
        }

        $allData = $this->index();


        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);

            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end




        // for each search field block start
        $allData = $this->index();

        foreach ($allData as $oneData) {
            $_allKeywords[] = trim($oneData->file_name);
        }
        $allData = $this->index();

        foreach ($allData as $oneData) {

            $eachString= strip_tags($oneData->file_name);
            $eachString=trim( $eachString);
            $eachString= preg_replace( "/\r|\n/", " ", $eachString);
            $eachString= str_replace("&nbsp;","",  $eachString);
            $WordsArr = explode(" ", $eachString);

            foreach ($WordsArr as $eachWord){
                $_allKeywords[] = trim($eachWord);
            }
        }
        // for each search field block end


        return array_unique($_allKeywords);


    }// get all keywords






}