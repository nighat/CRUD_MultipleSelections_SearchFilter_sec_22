-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 07, 2017 at 11:45 AM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `trainer_atomic_project_b44`
--
CREATE DATABASE IF NOT EXISTS `trainer_atomic_project_b44` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `trainer_atomic_project_b44`;

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE IF NOT EXISTS `birthday` (
`id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `date` varchar(112) NOT NULL,
  `soft_deleted` varchar(1122) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `name`, `date`, `soft_deleted`) VALUES
(2, 'nnn', '2017-02-08', 'Yes'),
(4, 'Nighat Parvin', '2017-02-08', 'No'),
(5, 'Nighat Parvin', '2017-02-08', 'No'),
(6, 'Birthday', '2017-02-09', 'No'),
(7, 'Birthday', '2017-02-16', 'No'),
(8, 'dhdd', '', 'No'),
(9, 'Birthday', '2017-02-13', 'No'),
(10, 'vv', '2017-02-08', 'No'),
(11, 'ddddd', '2017-02-08', 'No'),
(12, 'jkk', '2017-02-01', 'No'),
(13, 'hh', '2017-02-06', 'No'),
(14, 'Shamim', '2017-02-06', 'No'),
(15, 'Nighat Parvin', '2017-02-08', 'No'),
(16, 'asd', '2017-02-07', 'No'),
(17, 'shamim ', '2017-02-07', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `book_title`
--

CREATE TABLE IF NOT EXISTS `book_title` (
`id` int(11) NOT NULL,
  `book_name` varchar(11111) NOT NULL,
  `author_name` varchar(111) NOT NULL,
  `soft_deleted` varchar(111) DEFAULT 'no'
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `book_title`
--

INSERT INTO `book_title` (`id`, `book_name`, `author_name`, `soft_deleted`) VALUES
(1, 'nsa', 'n', ''),
(2, 'Nhddk', 'rttt', ''),
(3, 'dd', 'dddd  bgg', ''),
(4, 'dd', 'ss', 'Yes'),
(5, 'www', 'www', ''),
(6, 'gg', 'gg', 'Yes'),
(7, 'fff', 'fff', ''),
(8, 'bin', 'tap', ''),
(9, 'nighat parvion', 'bbbb', ''),
(11, 'Nighat Parvin', 'Hossain Book Dhaka', 'no'),
(12, 'Nighat Parvin', 'shamiha', 'yes'),
(13, 'Shamim miha', 'shamiha/shabiha', 'no'),
(14, 'Pinkey', 'shamiha/shabiha', 'yes'),
(15, 'siza', 'shamiha/shabiha', 'no'),
(16, 'Nhddk', 'kkkk', 'no'),
(17, 'nn', 'nn', 'Yes'),
(18, 'bbbb', 'g', 'no'),
(19, 'NMM', 'JIJ', 'no'),
(20, 'Nighat Parvin', 'bbbb', 'no'),
(21, 'Shamiha Binty Shamim', 'Shabiha', 'no'),
(23, 'Bin', 'BB', 'Yes'),
(24, 'bbbb', 'BB', 'Yes'),
(25, 'vvv', 'yuu', 'Yes'),
(27, 'natun', 'Shabiha', 'no'),
(28, 'jf', 'fff', 'no'),
(29, 'ff', 'ff', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`id` int(111) NOT NULL,
  `name` varchar(11) NOT NULL,
  `city` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`id`, `name`, `city`, `soft_deleted`) VALUES
(1, '', 'Kampong Soam', ''),
(2, '', 'Dhaka', ''),
(3, '', 'Phnom Penh', ''),
(4, '', 'Phnom Penh', ''),
(5, '', 'Kampong Soam', ''),
(8, '', 'Kampong Soam', 'no'),
(9, '', 'Dhaka', 'No'),
(10, '', 'Siem Reap', 'no'),
(11, '', 'Dhaka', 'no'),
(12, '', 'Siem Reap', 'no'),
(13, '', 'Siem Reap', 'no'),
(14, '', 'Kampong Soam', 'no'),
(15, '', 'Kampong Soam', 'No'),
(16, '', 'Kampong Soam', 'No'),
(17, '', 'Kampong Soam', 'No'),
(18, '', 'Siem Reap', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `email`
--

CREATE TABLE IF NOT EXISTS `email` (
`id` int(11) NOT NULL,
  `name` varchar(111) NOT NULL,
  `email` varchar(111) NOT NULL,
  `soft_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `email`
--

INSERT INTO `email` (`id`, `name`, `email`, `soft_deleted`) VALUES
(1, 'Nighat Parvin', 'nighatctg@gmail.com', 'No'),
(2, 'hanan', 'nighatctg@gmail.com', 'yes'),
(3, 'pinkey', 'pinkeyctg@gmail.com', 'No'),
(4, 'Shamiha Binty Shamim', 'shamiha@gmail.com', 'yes'),
(5, 'Shamiha Binty Shamim', 'shamiha@gmail.com', 'no'),
(6, 'Shamiha Binty Shamim', 'shamiha@gmail.com', 'No'),
(8, 'Nighat Parvin', 'nighat@gmail.com', 'no'),
(9, 'shamim', 'nighat@gmail.com', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
`id` int(11) NOT NULL,
  `name` varchar(11) NOT NULL,
  `gender` varchar(111) NOT NULL,
  `soft_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`id`, `name`, `gender`, `soft_deleted`) VALUES
(1, 'Nighat Parv', 'female', 'No'),
(2, 'Hasan', 'male', 'yes'),
(3, 'Shamiha Bin', 'female', 'yes'),
(4, 'Ruma Binty ', 'female', 'yes'),
(5, 'Ruma ', 'female', 'yes'),
(6, 'Sujana', 'female', 'no'),
(7, 'hanan', 'male', 'yes'),
(8, 'bbbbb', 'female', 'yes'),
(9, 'Nighat Parv', 'female', 'yes'),
(10, 'Nighat Parv', 'female', 'no'),
(11, 'Nighat Parv', 'female', 'yes'),
(12, 'gg', 'male', 'No'),
(13, 'Nighat Parv', 'female', 'yes'),
(14, 'new newre', 'female', 'no'),
(15, 'Nighat Parv', 'other', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `hobbies`
--

CREATE TABLE IF NOT EXISTS `hobbies` (
`id` int(11) NOT NULL,
  `name` varchar(11111) NOT NULL,
  `hobby` varchar(111) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `hobbies`
--

INSERT INTO `hobbies` (`id`, `name`, `hobby`, `soft_deleted`) VALUES
(1, 'Nighat Parvin', 'Array', 'yes'),
(2, 'Nighat Parvin', 'Reading', 'yes'),
(3, 'Nighat Parvin', 'Painting', 'yes'),
(4, 'bbbbb', 'Array', 'no'),
(5, 'bbbbb', 'Array', 'no'),
(6, 'Nighat Parvin', 'Array', 'no'),
(7, 'Shamiha Binty Shamim', 'Painting', 'yes'),
(8, 'new name', 'Reading', 'no'),
(9, '1st', 'Playing Guiter', 'no'),
(10, 'test name', 'Reading', 'no'),
(11, 'Shamiha ', 'Reading', 'no'),
(12, 'bbbbb', 'Playing Guiter,Reading', 'yes'),
(13, 'fF', 'Painting,Gardenning', 'no'),
(14, 'gg', 'Reading,Painting', 'no'),
(15, 'fff', 'Playing Guiter,Reading', 'no'),
(16, 'new newre', '', 'no'),
(17, 'kkk', 'Reading', 'yes'),
(18, 'ghhfhd', 'Reading', 'No'),
(19, 'hghhh', 'Playing Guiter,Reading', 'no'),
(20, 'fff', 'Playing Guiter', 'no');

-- --------------------------------------------------------

--
-- Table structure for table `profile_picture`
--

CREATE TABLE IF NOT EXISTS `profile_picture` (
`id` int(11) NOT NULL,
  `name` varchar(11111) NOT NULL,
  `file_name` varchar(111) NOT NULL,
  `soft_deleted` varchar(111) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profile_picture`
--

INSERT INTO `profile_picture` (`id`, `name`, `file_name`, `soft_deleted`) VALUES
(3, 'Shamiha Binty Shamim', 'DSC_0116.JPG', 'yes'),
(4, 'upload file in  uploads folder', 'upload.JPG', 'yes'),
(5, 'hanan', 'DSC_0534.JPG', 'yes'),
(6, 'Shamiha Binty Shamim', 'DSC_0107.JPG', 'yes'),
(7, '1st', 'DSC_0107.JPG', 'yes'),
(8, 'Nighat Parvin', 'DSC_0107.JPG', 'yes'),
(9, '1st', 'DSC_0107.JPG', 'yes'),
(10, 'Nighat Parvin', 'DSC_0131.JPG', 'Yes'),
(11, 'Nighat Parvin', 'DSC_0110.JPG', 'Yes'),
(12, 'Shamiha Binty Shamim', 'Untitled 5 .png', 'yes'),
(13, 'Nighat Parvin', 'Photo2170.jpg', 'yes'),
(14, 'Nighat Parvin', 'Photo2179.jpg', 'yes'),
(15, 'nnn', 'Photo1245_001.jpg', 'yes'),
(16, 'gggyyyyy', 'Photo1245_001.jpg', 'yes'),
(17, 'Birthday', 'DatabaseEngine.docx', 'Yes'),
(18, 'shabiha', 'Control Structure.docx', 'Yes'),
(19, 'Birthday', 'DatabaseBasics-DatabaseEngine-Relational  Database.docx', 'No');

-- --------------------------------------------------------

--
-- Table structure for table `summary`
--

CREATE TABLE IF NOT EXISTS `summary` (
`id` int(11) NOT NULL,
  `name` varchar(11) NOT NULL,
  `summery` varchar(11) NOT NULL,
  `soft_deleted` varchar(11) NOT NULL DEFAULT 'No'
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `summary`
--

INSERT INTO `summary` (`id`, `name`, `summery`, `soft_deleted`) VALUES
(1, 'Nighat Parv', 'ddd', 'yes'),
(2, 'bb', 'sss', 'yes'),
(3, 'hasan', 'hhjfdjdfjfj', 'yes'),
(5, 'tt', 't', 'no'),
(7, 'ggh', 'hgh', 'yes'),
(8, 'nighat- ---', 'ccccccccccc', 'No'),
(9, 'Nighat Parv', 'gff', 'yes'),
(11, 'hhh', 'ggggg', 'No');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `book_title`
--
ALTER TABLE `book_title`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email`
--
ALTER TABLE `email`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gender`
--
ALTER TABLE `gender`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hobbies`
--
ALTER TABLE `hobbies`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profile_picture`
--
ALTER TABLE `profile_picture`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `summary`
--
ALTER TABLE `summary`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `book_title`
--
ALTER TABLE `book_title`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `id` int(111) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `email`
--
ALTER TABLE `email`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `gender`
--
ALTER TABLE `gender`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `hobbies`
--
ALTER TABLE `hobbies`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `profile_picture`
--
ALTER TABLE `profile_picture`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `summary`
--
ALTER TABLE `summary`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
